<?php
/**
 * Created by PhpStorm.
 * User: zhc
 * Date: 2019/10/29
 * Time: 18:21
 */

namespace app\index\controller;


use think\App;
use think\Controller;

class Base extends Controller
{
    const SUCCESS_CODE = 1000; // 系统成功代码
    const ERROR_CODE = 1001; // 失败代码
    /**
     * @var \think\Request 请求体
     */
    var $request;
    /**
     * @var \library\Response 响应体
     */
    var $response;

    /**
     * @var boolean 是否强制登录
     */
    protected $_require_login = TRUE;
   public  function  __construct(App $app = null)
   {
       parent::__construct($app);
       $this->request  = request();
       $this->response = new \library\Response();
       if( $this->_require_login== TRUE && !session('kefu_name')){
           return  $this->redirect('login/Login');
       }
   }
}
