# timely客服系统
#### Timely交流群

QQ:850286851

#### 介绍
基于TP5.1+swoole写的客服系统，目前开放demo测试版 ，更多功能之后会基于此版本慢慢完善
如果喜欢请给打星

#### 软件架构
TP5.1+swoole
开源版：

后台登录：http://www.cnavd.com 登录账号：Timely 密码：123456

在后台添加客服

![add image](https://gitee.com/zhc02/timely_service/raw/master/public/static/common/images/hou.jpg)

添加客服成功之后  http://www.cnavd.com/index/kefu/index.html 登录客服工作平台

![add image](https://gitee.com/zhc02/timely_service/raw/master/public/static/common/images/kefuh.jpg)




商户版本：


客服登录地址：http://www.chat.cnavd.com/work/index/index/m/mc5dccfb80e7334

账号 ：客服一 密码：123456

账号 ：客服二 密码：123456

用户端测试地址 

http://www.chat.cnavd.com/  点击客服测试自动匹配客服服务



#### 安装教程

1.  下载本代码
2.  需要php7.2版本以上
3.  需要swoole4.3.2版本以上
#### 使用说明

1.   导入sql文件，修改config/databases.php,并搭建好nginx站点
2.   在项目根目录运行 mkdir -R 777 runtime
3.   在项目根目录运行php think chat start   支持 start | start -d |restart |stop
4.    然后浏览器访问 www.xxxx.com/index/kefu/index


